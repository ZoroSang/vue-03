import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

import VueLogin from "@/pages/VueLogin";
import router from "./router";
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import store from "./store";
import Vuex from "vuex";
Vue.use(Vuex);
Vue.component({VueLogin});
Vue.use(ElementUI);


import axios from "axios";
import VueAxios from "vue-axios";
Vue.prototype.$axios=axios

new Vue({
  render: h => h(App),
  router,
  axios,
  VueAxios,
  store
}).$mount('#app')
