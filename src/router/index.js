import Vue from "vue";
import VueRouter from "vue-router";
import VueLogin from "@/pages/VueLogin";
import VueIndex from "@/pages/VueIndex";
import VueReg from "@/pages/VueReg";
Vue.use(VueRouter)

export default new VueRouter({
    routes: [
        {
            path: "/",
            name: "VueLogin",
            component: VueLogin
        },
        {
            path: "/index",
            name: "VueIndex",
            component: VueIndex
        },
        {
            path: "/reg",
            name: "VueReg",
            component: VueReg
        }
    ]
})